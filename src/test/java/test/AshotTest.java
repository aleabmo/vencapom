package test.java.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class AshotTest extends BaseTest{
    private WebDriver driver;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     */

    /*
    Primeres proves per tal de conseguir un full page image i no partials com esta passant ara.
     */
    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() throws IOException {
        Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
        ImageIO.write(screenshot.getImage(), "PNG", new File("C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\fullpage.png"));
        driver.close();
    }

}

