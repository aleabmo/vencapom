package test.java.test;

import main.java.page.*;
import org.testng.annotations.Test;

public class CartTest extends BaseTest {
    LoginPage objLogin;
    PDPPage objPDP;
    PLPPage objPLP;
    HomePage objHomePage;
    CartPage objCart;
    CheckoutDeliveryPage objCheckout;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws InterruptedException 
     */

    @Test(priority=0)
    public void test_Home_Page_Appear_Correct() throws InterruptedException{
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
        driver.get("https://pre.venca.es/p/027653");
    	objPLP = new PLPPage(driver);
    	System.out.println(objPLP.getSimilarSize());
    	try {
    		objPLP.selectSize("3XL");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();
    	driver.get("https://pre.venca.es/cart");
    	objCart = new CartPage(driver);
    	System.out.println(objCart.getProductCount());

    	int index = objCart.getProductCount();
    	/* Manera de netejar el carrito*/
    	for(int i=0; i<index; i++)
    		objCart.selectProduct(0).sendToTrash();

    	/*
    	System.out.println(wea.getImg());
    	System.out.println(wea.getBrand());
    	System.out.println(wea.getDesc());
    	System.out.println(wea.getSeller());
    	System.out.println(wea.getRef());
    	System.out.println(wea.getAvailability());

    	System.out.println(wea.getColor());
    	System.out.println(wea.getSize());
    	System.out.println(wea.getQt());
    	System.out.println(wea.getPrice());
    	*/
    	//If we wanna add to list
    	//wea.addToWish();
    	//objCheckout = objCart.goToCheckout();
    	driver.close();
    }

}