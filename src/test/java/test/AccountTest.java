package test.java.test;

import main.java.page.*;
import org.testng.annotations.Test;

public class AccountTest extends BaseTest {
    LoginPage objLogin;
    PDPPage objPDP;
    PLPPage objPLP;
    HomePage objHomePage;
    RegisterPage objGuest;
    AccountDataPage objAccount;
    AccountOrdersPage objAccountOrders;
    AccountPassPage objAccountPass;
    AccountDataModPage objAccountMod;


    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException Exception
     */

    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() {
    	
    	objPLP = new PLPPage(driver);
        driver.get("https://pre.venca.es/login");
    	objLogin = new LoginPage(driver);
    	objHomePage = objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
    	//objGuest = objLogin.goToRegister();
    	//objGuest.registerToVenca("testingvencaw@gmail.com", "123456", "wea", "wea", "1", "Enero", "1990", 'M');
    	driver.get("https://pre.venca.es/mi-cuenta");
    	objAccount = new AccountDataPage(driver);
    	objAccountOrders = objAccount.goToOrders();
    	System.out.println(objAccountOrders.countOrders());
    	System.out.println(objAccountOrders.getOrder(0).getRef());
    	//objAccountMod = objAccount.goToModify();
    	//objAccountMod.inputData("a", "a", "opt", "city", "43005", "wea", "");
    	//driver.get("https://pre.venca.es");

    	driver.close();

    	

    }

}