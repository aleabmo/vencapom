package test.java.test;

import main.java.page.HomePage;
import main.java.page.LoginPage;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoMoreOrdersException 
     */

    @Test(priority=1)
    public void test_login_is_correct() {
		driver.get("https://pre.venca.es/login");
		objLogin = new LoginPage(driver);
    	System.out.println(objLogin.getTitle());
    	System.out.println(objLogin.getUrl());
    	objLogin.goToRegister();
        driver.get("https://pre.venca.es/login");
    	objLogin = new LoginPage(driver);
    	objLogin.gotToRecovery();
        driver.get("https://pre.venca.es/login");
    	objLogin = new LoginPage(driver);
    	objHomePage = objLogin.loginToVenca("itfrontoffice@venca.es", "123456");
    	
    	driver.close();
    }
}