package test.java.test;

import main.java.page.PLPPage;
import org.testng.annotations.Test;
import utils.TakeScreen;

public class ImageMaskParserTest extends BaseTest {

    /**
     * This test go to https://pre.venca.es
     * Testing if taking screen is working properly.
     * Generic testing
     * @throws Exception
     */
    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() throws Exception {
        int x=382, y=286;
        int width=1525, height=872;

        PLPPage objPLPPage = new PLPPage(driver);
        TakeScreen.takeSnapShot(driver, "C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\pro_plp.png") ;

        /*
        ImageMask.setColor(Color.white);
        ImageMask mask = new ImageMask("C:\\\\Users\\\\AleixAbengochea\\\\Desktop\\\\QA_Aleix\\\\scripts\\\\imgdiff\\\\pre_help.json");
        System.out.println(mask);
        ImageMask.applyMask("C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\scripts\\imgdiff\\pre_help.png", mask);

         */

        driver.close();
    }

}
