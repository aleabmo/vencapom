package test.java.test;

import main.java.exception.NoProductException;
import main.java.page.PDPPage;
import org.testng.annotations.Test;

public class PLPTest extends BaseTest {
    PDPPage objPDP;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws NoProductException 
     */

    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() throws NoProductException {

    	driver.get("https://pre.venca.es/e/38");
    	objPDP = new PDPPage(driver);
    	System.out.println(objPDP.getHeader());
    	System.out.println(objPDP.getDescription());
    	System.out.println(objPDP.getNumProd());
    	objPDP.setProduct(1);
    	System.out.println(objPDP.getBrandProd());
    	System.out.println(objPDP.getDescProd());
    	for(String tag : objPDP.getTagsProd())
    		System.out.print(tag + ", ");
    	System.out.println();
    	System.out.println(objPDP.getImgProd());
		objPDP.addProdToWish();
		objPDP.clickProd();
    	driver.close();

    	

    }

}