package test.java.test;

import main.java.page.HomePage;
import main.java.page.LoginPage;
import main.java.page.RecoveryPage;
import org.testng.annotations.Test;

public class RecoveryTest extends BaseTest {
    LoginPage objLogin;
    HomePage objHomePage;
    RecoveryPage objRecovery;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     */

    @Test(priority=1)
    public void test_Home_Page_Appear_Correct() {
        driver.get("https://pre.venca.es/login");
    	objLogin = new LoginPage(driver);
    	objRecovery = objLogin.gotToRecovery();
    	objRecovery.setEmail("wea@wea.es");
    	objRecovery.clickSend();
    	driver.close();
    }
}