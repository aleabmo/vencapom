package test.java.test;

import java.util.concurrent.TimeUnit;

import main.java.page.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PDPTest extends BaseTest {
    LoginPage objLogin;
    PLPPage objPLP;
    CartPage objCart;
    CheckoutDeliveryPage objCheckout;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws InterruptedException 
     */

    @Test(priority=0)
    public void test_Home_Page_Appear_Correct() throws InterruptedException{

       //Create Login Page object

    //objLogin = new LoginPage(driver);
		driver.get("https://pre.venca.es/p/117003");
    	objPLP = new PLPPage(driver);
    	System.out.println(objPLP.getSimilarSize());
    	try {
    		objPLP.selectSize("XS");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();
    	driver.get("https://pre.venca.es/cart");
    	objCart = new CartPage(driver);
    	objCheckout = objCart.goToCheckout();
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("aleix.abengochea@gmail.com", "123456");

    }

}