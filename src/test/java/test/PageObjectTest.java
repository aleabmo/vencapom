package test.java.test;

import main.java.page.HomePage;
import org.testng.annotations.Test;

public class PageObjectTest extends BaseTest {
    HomePage objHomePage;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     */

    @Test(priority=1)
    public void test_login_is_correct() {
    	objHomePage = new HomePage(driver);
    	System.out.println(objHomePage.getTitle());
    	System.out.println(objHomePage.getUrl());
    	driver.close();
    }

}