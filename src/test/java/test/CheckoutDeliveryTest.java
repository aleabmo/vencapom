package test.java.test;

import main.java.exception.AlreadyLoggedException;
import main.java.exception.IsDisabledException;
import main.java.page.*;
import main.java.page.modal.AddressCheckoutModal;
import org.testng.annotations.Test;

public class CheckoutDeliveryTest extends BaseTest {
    LoginPage objLogin;
    PDPPage objPDP;
    PLPPage objPLP;
    HomePage objHomePage;
    CartPage objCart;
    CheckoutDeliveryPage objCheckout;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws InterruptedException 
     */

    @Test(priority=0)
    public void test_Home_Page_Appear_Correct() throws InterruptedException, AlreadyLoggedException, IsDisabledException {

    	//Home
		driver.get("https://pre.venca.es");
    	objHomePage = new HomePage(driver);

    	//Login
    	objHomePage.clickLogin();
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("pilarecargable7@gmail.com", "iniesta7");

    	//PLP
    	driver.get("https://pre.venca.es/p/027653");
    	objPLP = new PLPPage(driver);
    	try {
    		objPLP.selectSize("XS");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();

    	//Cart
    	driver.get("https://pre.venca.es/cart");
    	objCart = new CartPage(driver);
    	//System.out.println(objCart.getProductCount());
    	//Product wea = objCart.selectProduct(0);
    	//System.out.println(wea);
    	objCheckout = objCart.goToCheckout();
    	/*
    	System.out.println(objCheckout.getName());
    	System.out.println(objCheckout.getEmail());
    	System.out.println(objCheckout.getItemsPrice());
    	System.out.println(objCheckout.getDeliveryPrice());
    	System.out.println(objCheckout.getPaymentPrice());
    	System.out.println(objCheckout.getTotalPrice());

    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	*/
    	objCheckout.setPhone("612123553");
    	/*
    	PuntoPackCheckoutModal wea = objCheckout.searchPack();
    	wea.setCp("");
    	wea.setCity("");
    	wea.clickSearch();
    	wea.clickClose();
    	/*
    	System.out.println(wea.getNumOptions());
        System.out.println(wea.getOption(2));
        wea.setOption(2);
    	 */


    	/*
    	objCheckout.selectUrgent();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

    	objCheckout.selectHome();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

*/
    	objCheckout.selectCorreos();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	AddressCheckoutModal weo = objCheckout.changeAddress();

    	weo.clearAll();

    	weo.setTitle("wea");
    	weo.setAddress("Vidal i barraquer");
    	weo.setNum("3");
    	weo.setCity("tarragona");
    	weo.setCP("43005");
    	weo.setProv("Tarragona");


    	weo.clickAddAddress();
    	objCheckout.setPhone("612123553");

    	CheckoutPaymentPage objPayment = objCheckout.goToPayment();
    	//driver.close();
    	/*
    	System.out.println(objPayment.getName());
    	System.out.println(objPayment.getEmail());
    	System.out.println(objPayment.getItemsPrice());
    	System.out.println(objPayment.getDeliveryPrice());
    	System.out.println(objPayment.getPaymentPrice());
    	System.out.println(objPayment.getTotalPrice());

    	objPayment.setCreditCard("4012001037141112", "12/21", "123", "Test test", false);
    	*/
    	objPayment.setCardNum("4012001037141112");
    	objPayment.setCardExp("11/21");
    	objPayment.setCardCVC("123");
    	objPayment.setCardName("test test");
    	//objPayment.setCardSave();
    	objPayment.goToFinishPayment();


    }

}