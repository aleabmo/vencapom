package test.java.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import utils.Constants;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;

    @BeforeTest
    public void setup() throws MalformedURLException {

        //System.setProperty("webdriver.chrome.driver","C:\\Users\\AleixAbengochea\\Downloads\\chromedriver_win33\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver","/home/bubuwuena/selenium/chromedriver");
        driver = new ChromeDriver();
        //driver = new FirefoxDriver();


        // Changed system in order to get constants from centralized file
        //System.setProperty("webdriver.chrome.driver","C:\\Users\\AleixAbengochea\\Downloads\\chromedriver_win33\\chromedriver.exe");
//        chromeOptions.addArguments("--headless");
        /*
        System.setProperty("webdriver.chrome.driver", Constants.DRIVERPATH);
        ChromeOptions chromeOptions = new ChromeOptions();
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub/"), chromeOptions);
         */

        //Implicit wait per tal de esperar a que carreguin les pagines.
        driver.manage().timeouts().implicitlyWait(Constants.TIMEOUT, TimeUnit.SECONDS);
        //Maximitza la finestra del explorador.
        driver.manage().window().maximize();

        // Es necessari anar algun lloc de inici?
        // Seria mes retable setejar un config a les ip de serie i demas
        //driver.get("https://pre.venca.es/");
    }
}
