package test.java.test;

import main.java.exception.IsDisabledException;
import main.java.page.*;
import org.testng.annotations.Test;

public class FullBuyTest extends BaseTest{
    LoginPage objLogin;
    PLPPage objPLP;
    ThanksPage objThanksPage;
    CartPage objCart;
    CheckoutDeliveryPage objCheckout;

    /**
     * This test go to https://pre.venca.es
     * Verify the home page using Dashboard message
     * Generic testing
     * @throws InterruptedException 
     */

    /*
    @Test(priority=0)
    public void test_Checkout_PuntoPack_Visa() throws InterruptedException, IsDisabledException{
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("irduca@gmail.com", "123456");

    	driver.get("https://pre.venca.es/p/027653");
    	objPLP = new PLPPage(driver);
    	System.out.println(objPLP.getSimilarSize());
    	try {
    		objPLP.selectSize("4XL");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();
    	objPLP.clickCart();
    	objCart = new CartPage(driver);
    	objCheckout = objCart.goToCheckout();
    	

    	objCheckout.selectCorreos();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

    	CheckoutPaymentPage objPayment = objCheckout.goToPayment();
    	objPayment.setDNI("12345678Z");
    	objThanksPage = objPayment.goToFinishPayment();

    	System.out.println(objThanksPage.getOrderId());
    	System.out.println(objThanksPage.getEmail());
    	System.out.println(objThanksPage.getNumPack());
    	System.out.println("=== PACKET ===");
    	System.out.println(objThanksPage.getPacket(0));
    	System.out.println("=== ITEM ===");
    	System.out.println(objThanksPage.getPacket(0).getItem(0));
    	System.out.println("============");
    	System.out.println(objThanksPage.getPaymentType());
    	System.out.println(objThanksPage.getCardNumber());
    	System.out.println(objThanksPage.getPaymentPrice());
    	System.out.println(objThanksPage.getTotalPrice());
    	System.out.println(objThanksPage.getTotalPaquetsPrice());
    	
    	driver.close();
    }
    */

@Test(priority=0)
    public void test_Checkout_Correos_Cash() throws  IsDisabledException {
		driver.get("https://pre.venca.es/login");
    	objLogin = new LoginPage(driver);
    	objLogin.loginToVenca("aleix.abengochea@gmail.com", "111111");

    	driver.get("https://pre.venca.es/p/032264");
    	objPLP = new PLPPage(driver);
    	System.out.println(objPLP.getSimilarSize());
    	try {
    		objPLP.selectSize("36");
    		} catch (Exception e) {
    			
    		}
    	objPLP.addCart();
    	objPLP.clickCart();
    	objCart = new CartPage(driver);
    	objCheckout = objCart.goToCheckout();
    	
    	objCheckout.selectCorreos();
    	System.out.println(objCheckout.getText());
    	System.out.println(objCheckout.getAddress());
    	objCheckout.setPhone("612123553");

    	CheckoutPaymentPage objPayment = objCheckout.goToPayment();
    	//objPayment.setCash();
    	objPayment.setCreditCard("4263970000005262","12/21","222","aldfkj kadfjl0", false);
    	objThanksPage = objPayment.goToFinishPayment();

    	System.out.println(objThanksPage.getOrderId());
    	System.out.println(objThanksPage.getEmail());
    	System.out.println(objThanksPage.getNumPack());
    	System.out.println("=== PACKET ===");
    	System.out.println(objThanksPage.getPacket(0));
    	System.out.println("=== ITEM ===");
    	System.out.println(objThanksPage.getPacket(0).getItem(0));
    	System.out.println("============");
    	System.out.println(objThanksPage.getPaymentType());
    	System.out.println(objThanksPage.getCardNumber());
    	System.out.println(objThanksPage.getPaymentPrice());
    	System.out.println(objThanksPage.getTotalPrice());
    	System.out.println(objThanksPage.getTotalPaquetsPrice());


    	driver.close();
    }
}