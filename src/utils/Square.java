package utils;

import javax.swing.*;
import java.awt.*;

public class Square extends JPanel {
    public int x; int y; int x2; int y2;

    public Square(int x, int y, int x2, int y2){
        this.x = x;
        this.y = y;
        this.x2 = x2;
        this.y2 = y2;
    }

    public int getPX(){
        return Math.min(x, x2);
    }

    public int getPY(){
        return Math.min(y, y2);
    }

    public int getPW() {
        return Math.abs(x-x2);
    }

    public int getPH() {
        return Math.abs(y-y2);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.drawRect(getPX(), getPY(), getPW(), getPH());
        g.drawRect(getPX(),getPY(), 10,10);
    }

    @Override
    public boolean contains(int x, int y) {
        if ( x > getPX() && x < getPX()+10 && y > getPY() && y < getPY()+10 )
            return true;
        else
            return false;
    }
}
