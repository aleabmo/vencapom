package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Carousel {

	private WebElement cr;

	public Carousel(WebElement cr) {
		this.cr = cr;
	}
	
	public void goPrev() {
		try {
			cr.findElement(By.className("//a[@data-slide='prev']")).click();
		}catch(Exception e) {
			
		}
	}

	public void goNext() {
		try {
			cr.findElement(By.xpath("//a[@data-slide='next']")).click();
		}catch(Exception e) {

		}
	}
	
	public WebElement getActive() {
		return cr.findElement(By.xpath("//div[@class='item active']"));
	}
}
