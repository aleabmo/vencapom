package utils;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * Clase per tal de fer abstraccio del dropdown, totalment millorable ja que tenim xpaths
 * @author aleixabengochea
 *
 */

public class DropDown {
	private WebDriver driver;
	private String xpath;

	public DropDown(WebDriver driver, String xpath) {
    		this.driver = driver;
    		this.xpath = xpath;
    		PageFactory.initElements(driver, this);
	}
	
	/**
	 * Funcio que retorna tots els elements que hi ha al dropdown
	 * @return List of strings with the options of dropdown
	 */
	
	public List<String> listElem(){
		List<String> result = new ArrayList<>();
		//Obrim la llista
		driver.findElement(By.xpath(xpath+"/button")).click();
		//Obtenim tots els elements
		List<WebElement> drop = driver.findElements(By.xpath(xpath+"/ul/li/span"));
		for(WebElement aux : drop) {
			result.add(aux.getText());
		}
		//Tanquem la llista ja que no agafem cap
		driver.findElement(By.xpath(xpath+"/button")).click();
		return result;

		//driver.findElement(By.xpath("//span[contains(@data-sort, 'new')]")).click();
	}
	
	//Millorable amb excepcio propia
	/**
	 * Funcion que permite una opcion del dropdown
	 * @param optn String naming option you wanna choose
	 */

	public void getOption(String optn) {
		if(listElem().contains(optn)) {
			driver.findElement(By.xpath(xpath+"/button")).click();
			driver.findElement(By.xpath(xpath+"//span[contains(text(), '"+optn+"')]")).click();
		} else {
			System.out.println("Option not in dropdown");
		}
	}
}
