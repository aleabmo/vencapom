package main.java.exception;

public class NoProductException extends Exception {
	private static final long serialVersionUID = 1L;

	//Exception for no product on PDPPage
	public NoProductException(String errMsg) {
		super(errMsg);
	}
}
