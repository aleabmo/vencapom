package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountDataModPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(xpath="/html/body/main/main/section/div/form/div[1]/div[5]/div/label")
    WebElement btnMoreData;

    @FindBy(id="Address_Street")
    WebElement inputAddress;

    @FindBy(id="Address_Number")
    WebElement inputNum;

    @FindBy(id="Address_Additional")
    WebElement inputOpcional;

    @FindBy(id="Address_City")
    WebElement inputCity;

    @FindBy(id="Address_PostalCode")
    WebElement inputCP;

    @FindBy(id="Address_Province")
    WebElement inputProv;

    @FindBy(id="Phone")
    WebElement inputPhone;

    @FindBy(xpath="/html/body/main/main/section/div/form/div[3]/div/button")
    WebElement btnValidate;
    
    public void loadMoreData() {
    	WebDriverWait wait = new WebDriverWait(driver, 30);
    	wait.until(ExpectedConditions.visibilityOf(inputAddress));
    	btnMoreData.click();
    }

    public AccountDataModPage(WebDriver driver){
    	super(driver);
    }
    
    public void inputAddress(String address) {
    	inputAddress.sendKeys(address);
    }

    public void inputNum(String num) {
    	inputNum.sendKeys(num);
    }

    public void inputOpcional(String opt) {
    	inputOpcional.sendKeys(opt);
    }

    public void inputCity(String city) {
    	inputCity.sendKeys(city);
    }

    public void inputCP(String cp) {
    	inputCP.sendKeys(cp);
    }
    
    public void inputProv(String prov) {
    	inputProv.sendKeys(prov);
    }

    public void inputPhone(String phone) {
    	inputPhone.sendKeys(phone);
    }
    
    public AccountDataPage clickValidate() {
    	btnValidate.click();
    	return new AccountDataPage(driver);
    }
    
    public AccountDataPage inputData(String address, String num, String opt, String city, String cp, String prov, String phone) {
    	btnMoreData.click();
    	//Entre que se obra el modal i no
    	WebDriverWait wait = new WebDriverWait(driver, 30);
    	wait.until(ExpectedConditions.visibilityOf(inputAddress));

    	inputAddress.sendKeys(address);
    	inputNum.sendKeys(num);
    	inputOpcional.sendKeys(opt);
    	inputCity.sendKeys(city);
    	inputCP.sendKeys(cp);
    	inputProv.sendKeys(prov);
    	inputPhone.sendKeys(phone);
    	btnValidate.click();
    	return new AccountDataPage(driver);
    }
}
