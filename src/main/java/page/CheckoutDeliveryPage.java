package main.java.page;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.java.page.modal.AddressCheckoutModal;
import main.java.page.modal.PuntoPackCheckoutModal;
import utils.Constants;

public class CheckoutDeliveryPage extends CheckoutPage {

    @FindBy(xpath="\\a[contains(@href,'#mapmodals')]")
    WebElement modalPuntopack;
    
    @FindBy(xpath="//div[@id='deliveryTypes']/div")
    List<WebElement> listDelivery;
    
    @FindBy(id="heading-feP")
    WebElement radioPack;

	@FindBy(xpath="//*[@id='divAddDeliveryAddressPackPoint']/a")
	WebElement btnSearchPack;

	@FindBy(id="heading-fe3")
    WebElement radioExpress;

    @FindBy(id="heading-fe1")
    WebElement radioHome;

    @FindBy(id="heading-fe0")
    WebElement radioPost;

    @FindBy(id="paymentsStepButton")
    WebElement btnPayment;
    
    private String deliveryOption;

	public CheckoutDeliveryPage(WebDriver driver) {
		super(driver);
		deliveryOption = "P";
	}
	
	/**
	 * Selects a puntopack option
	 */
	public void selectPuntopack(){
		radioPack.click();
		deliveryOption = "P";
	}

	public PuntoPackCheckoutModal searchPack(){
	    btnSearchPack.click();
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("mapmodals"))
		);
		return new PuntoPackCheckoutModal(this);
	}

	/**
	 * Selects a Urgente option
	 */
	public void selectUrgent(){
		radioExpress.click();
		deliveryOption = "3";
	}

	/**
	 * Selects a Domicilio option
	 */
	public void selectHome(){
		radioHome.click();
		deliveryOption = "1";
	}

	/**
	 * Selects a Correos option
	 * @return 
	 */
	public void selectCorreos(){
		//radioPost.click();
		ajaxWait(radioPost, 10);
		deliveryOption = "0";
	}
	
	/**
	 * Returns description of current method
	 * @return String with description
	 */
	public String getText() {
		WebElement selected;
		if (deliveryOption.equals("P"))
			selected = driver.findElement(By.cssSelector("#packPointSelected > p"));
		else
			selected = driver.findElement(By.cssSelector("#collapse-fe"+ deliveryOption +" > .panel-body > p"));
		return selected.getText();
	}
	
	/**
	 * Method to get the current address
	 * @return String with hole address
	 */
	public String getAddress() {
WebElement selected;
		if (deliveryOption.equals("P"))
			selected = driver.findElement(By.cssSelector(".col-md-6"));
		else
			selected = driver.findElement(By.cssSelector("#collapse-fe"+ deliveryOption +" .greyPanel"));
		return selected.getText();
	}

	/**
	 * Method to open modal in order to change the current address
	 */
	public AddressCheckoutModal changeAddress() {
		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);


		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("myModalWait")));
		} catch(TimeoutException e){
			System.out.println("Modal not found");
		}


		wait.until(ExpectedConditions.and(
				ExpectedConditions.invisibilityOfElementLocated(By.id("myModalWait")),
				ExpectedConditions.invisibilityOfElementLocated(By.id("myModal")),
				ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='collapse-fe"+ deliveryOption +"']/div/div[3]/a"))
				//ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class, 'modal-backdrop fade')]"))

		));

		WebElement aux = driver.findElement(By.xpath("//div[@id='collapse-fe"+ deliveryOption +"']/div/div[3]/a"));
		aux.click();
		aux.click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("changeDeliveryAddress")));

		return new AddressCheckoutModal(this);
	}
	
	public void getHorario() {
		WebElement selected;
		if (deliveryOption.equals("P")) {
			selected = driver.findElement(By.cssSelector(".fa-clock-o"));
			selected.click();
		}
	}
	
	/**
	 * Sets a new phone on current selected method
	 * @param num String with phone number
	 */
	public void setPhone(String num) {
		WebElement selected;
		if (deliveryOption.equals("P"))
			selected = driver.findElement(By.id("PhoneToValidateP"));
		else
			selected = driver.findElement(By.cssSelector("#collapse-fe"+ deliveryOption +" #PhoneToValidate"+ deliveryOption));

		WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
	    wait.until(ExpectedConditions.and(
				ExpectedConditions.invisibilityOfElementLocated(By.id("myModal")),
				ExpectedConditions.invisibilityOfElementLocated(By.id("myModalWait")),
				ExpectedConditions.invisibilityOfElementLocated(By.id("changeDeliveryAddress")),
				ExpectedConditions.elementToBeClickable(selected)
		));

	    selected.sendKeys(num);
	}

	//prova ajax handling
	private void ajaxWait(WebElement button, int timeout) {
		String source = driver.getPageSource();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout));
		button.click();
		
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				return !source.equals(driver.getPageSource());
			}
		});
	}

	/**
	 * Method which clicks on button to go to payment tab
	 * @return A CheckoutPaymentPage in order to cast methods
	 */
	public CheckoutPaymentPage goToPayment() {
		btnPayment.click();
		return new CheckoutPaymentPage(driver);
		
	}
}
