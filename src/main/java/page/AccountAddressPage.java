package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountAddressPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="myorders")
    WebElement ordersTab;

    public AccountAddressPage(WebDriver driver){
    	super(driver);
    }
}
