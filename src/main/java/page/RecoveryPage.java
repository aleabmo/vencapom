package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RecoveryPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="EmailRegister")
    WebElement email;

    @FindBy(xpath="/html/body/main/section/section/form/div/button")
    WebElement btnSend;    

    public RecoveryPage(WebDriver driver){
    	super(driver);
    }

    /**
     * Set email from account to recover.
     * @param strEmail Email from account you want to recover
     */
    public void setEmail(String strEmail){
        email.sendKeys(strEmail);     
    }

    /**
     * Click on send button
     */
    public void clickSend(){
            btnSend.click();
    }  

}