package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPassPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="PasswordToModify")
    WebElement actualPass;

    @FindBy(id="Password")
    WebElement newPass;

    @FindBy(id="ConfirmPassword")
    WebElement confirmPass;

    @FindBy(xpath="//button")
    WebElement btnPass;

    public AccountPassPage(WebDriver driver){
    	super(driver);
    }
    
    public void inputActualPass(String pass) {
    	actualPass.sendKeys(pass);
    }

    public void inputNewPass(String pass) {
    	newPass.sendKeys(pass);
    }

    public void inputConfirmPass(String pass) {
    	confirmPass.sendKeys(pass);
    }

    public AccountDataPage sendForm() {
    	btnPass.click();
    	return new AccountDataPage(driver);
    }

    /**
     * Method for changing account password
     * @param actualPass String with actual password
     * @param newPass String with new password you want to set
     */
    public void changePass(String actualPass, String newPass) {
    	inputActualPass(actualPass);
    	inputNewPass(newPass);
    	inputConfirmPass(newPass);
    	sendForm();
    }

}
