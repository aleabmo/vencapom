package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountCommunicationPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="myorders")
    WebElement ordersTab;

    public AccountCommunicationPage(WebDriver driver){
    	super(driver);
    }
}
