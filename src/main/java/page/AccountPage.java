package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class AccountPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="myorders")
    WebElement ordersTab;

    @FindBy(id="mydata")
    WebElement infoTab;

    @FindBy(id="myaddresses")
    WebElement addressTab;

    @FindBy(id="mypayments")
    WebElement paymentTab;

    @FindBy(id="mypassword")
    WebElement passTab;

    @FindBy(id="myprivacity")
    WebElement privacityTab;

    @FindBy(id="myContactPreferences")
    WebElement communicationTab;

    private String url = "/mi-cuenta";

    public AccountPage(WebDriver driver){
    	super(driver);
    }

    //Set user name in textbox
    public AccountOrdersPage goToOrders() {
        ordersTab.click();     
        return new AccountOrdersPage(driver);
    }

    //Set user name in textbox
    public AccountDataPage goToData(){
        infoTab.click();     
        return new AccountDataPage(driver);
    }

    //Set user name in textbox
    public AccountAddressPage goToAddress(){
        addressTab.click();     
        return new AccountAddressPage(driver);
    }

    //Set user name in textbox
    public AccountPaymentPage goToPayment(){
        paymentTab.click();     
        return new AccountPaymentPage(driver);
    }

    //Set user name in textbox
    public AccountPassPage goToPass(){
        passTab.click();     
        return new AccountPassPage(driver);
    }

    //Set user name in textbox
    public AccountPrivacityPage goToPrivacity(){
        privacityTab.click();     
        return new AccountPrivacityPage(driver);
    }

    //Set user name in textbox
    public AccountCommunicationPage goToCommunication(){
        communicationTab.click();     
        return new AccountCommunicationPage(driver);
    }
}
