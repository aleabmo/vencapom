package main.java.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.java.exception.ElementNotFoundException;
import main.java.exception.NoPreviousPriceException;
import main.java.exception.NoRatingException;
import main.java.exception.NoSignException;
import main.java.exception.SizeAgotadoException;

public class PLPPage extends MenuPageObject {

    @FindBy(id="productThumbnails")
    WebElement carrousel;

    @FindBy(id="mainProductImage")
    WebElement img;

    @FindBy(id="carrousel-tag")
    WebElement tags;

    @FindBy(xpath="//span[contains(@class, 'sellerTitle')]")
    WebElement sellerTitle;

    @FindBy(xpath="//*[@id=\"mainTextFeatures\"]/div/h1")
    WebElement title;

    @FindBy(id="productprice")
    WebElement price;

    @FindBy(id="previousPriceText")
    WebElement previousPrice;

    @FindBy(id="reviewsAside")
    WebElement rating;

    @FindBy(id="colorName")
    WebElement currentColor;

    @FindBy(xpath="//button[contains(@class, 'colorPicker')]")
    List<WebElement> btnColors;

    @FindBy(id="sizes")
    WebElement size;

    @FindBy(xpath="//a[contains(@data-target, '#modalSizeGuide')]")
    WebElement sizeguide;

    @FindBy(id="dropdownSize")
    WebElement sizedp;

    @FindBy(className="add")
    WebElement btnAdd;

    @FindBy(id="addToWishList")
    WebElement btnWishList;

    @FindBy(xpath="//*[@id=\"asideFeatures\"]/p/span")
    WebElement signText;

    @FindBy(id="mainProduct")
    WebElement tabs;

    @FindBy(id="syte-shop-the-look-container")
    WebElement completaLook;

    @FindBy(id="syte-similar-items-container")
    WebElement similarProducts;

    @FindBy(id="recommendedProducts")
    WebElement recomendedProducts;

	public PLPPage(WebDriver driver) {
		super(driver);
		
	}
	
	/**
	 * Method for changing img of carrousel
	 * @param index Integer of img in carrousel
	 */
	public void changeImg(int index) {
		List<WebElement> aux = carrousel.findElements(By.xpath(".//a/img"));
		try {
			aux.get(index).click();
		}catch(Exception e) {
			System.out.println("Index out of bounds");
		}
		
	}
	
	/**
	 * Function that return number of img on carrousel.
	 * @return
	 */
	public int numOfImg() {
		return carrousel.findElements(By.xpath(".//a/img")).size();
	}
	
	/**
	 * Method to make zoom on current img
	 */
	public void zoomImg() {
		img.click();
	}
	
	/**
	 * Method to choose color based on color name.
	 * @param color
	 */
	public void changeColor(String color) {
		for(WebElement aux : btnColors) {
			if(color.equalsIgnoreCase(aux.getText())){
				aux.click();
				break;
			}
		}
	}
	
	/**
	 * Method to add item to cart. !!Care with size modal.
	 */
	public void addCart() {
		btnAdd.click();
		//Wait for Cesta modal to apper in order to actually get items on cart.
		// Es necessari, si no pot ser que vagi al cart i no hi ha hagi items.
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addProd")));
	}
	
	/**
	 * Method to add item to wishlist.
	 */
	public void addWhishList() {
		btnWishList.click();
	}

	/**
	 * Mthod to get selleritle
	 * @return String containing seller name.
	 */
	public String getSellerTitle() {
		return sellerTitle.getText();
	}
	
	/**
	 * Method to get title of product
	 * @return String that contains text.
	 */
	public String getTitle() {
		return title.getText();
	}
	
	/**
	 * Method which returns Current price. 
	 * @return String with current price.
	 */
	public String getPrice() {
		return price.getText();
	}

	/**
	 * Method that returns previous price if it has one
	 * @return String with previous price.
	 */
	public String getPreviousPrice() throws NoPreviousPriceException{
		try {
			return previousPrice.getText();
		} catch(NoSuchElementException e) {
			throw new NoPreviousPriceException("This product has no previous price");
		}
	}

	/**
	 * Method which returns current Color.
	 * @return
	 */
	public String getColor() {
		return currentColor.getText();
	}
	
	/**
	 * Method that return list of colors on product
	 * @return List of strings containing colors.
	 */
	public List<String> getColors() {
		List<String> result = new ArrayList<>();
		for(WebElement aux : btnColors) {
			result.add(aux.getText());
		}
		return result;
	}

	/**
	 * Method 
	 * @return
	 */
	public String getSign() throws NoSignException {
		try {
			return signText.getText();
		} catch(NoSuchElementException e) {
			throw new NoSignException("This product has no sign");
		}
	}
	
	/**
	 * Method which return number of reviews
	 * Thorws Exception if product has no rating
	 * @return String containing number of reviews
	 */
	public String getRating() throws NoRatingException {
		try {
			return rating.findElement(By.xpath(".//div/div/span")).getText();
		}catch(NoSuchElementException e) {
			throw new NoRatingException("This product has no ratings");
		}
	}
	
	/**
	 * Method that returns the current tags applied
	 * @return List of strings with tags type.
	 */
	public List<String> getTags(){
		List<String> result = new ArrayList<>();
		List<WebElement> temp = tags.findElements(By.xpath(".//div"));
		for(WebElement aux : temp) {
			result.add(aux.getText());
		}
		return result;
	}
	
	/**
	 * Method for getting size on product
	 * @param s
	 * @throws Exception
	 */
	public void selectSize(String s) throws Exception{
		sizedp.click();
		WebElement aux = null;
		try {
			aux = size.findElement(By.xpath(".//li/a[contains(@data-id,'"+s+"')]"));
		}catch(WebDriverException e) {
			throw new ElementNotFoundException("Xpath not found");
		}
		try {
			aux.click();
		}catch(WebDriverException e) {
			throw new SizeAgotadoException("La talla "+s+" esta agotada.");
		}
	}
	
	/**
	 * Method for getting syte products list size
	 * @return
	 */
	public int getLookSize() {
		return completaLook.findElements(By.xpath(".//div[contains(@class,'owl-item')]")).size();
	}
	
	/**
	 * Method for getting syte similar products list size
	 * @return
	 */
	public int getSimilarSize() {
		return similarProducts.findElements(By.xpath(".//div[contains(@class,'owl-item')]")).size();
	}
	
	/**
	 * Method for getting recommended list size
	 * @return
	 */
	public int getRecommnedSize() {
		return recomendedProducts.findElements(By.xpath("./div[2]/div/div/div")).size();
	}
}
