package main.java.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AccountOrdersPage extends AccountPage {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="myorders")
    WebElement ordersTab;

    @FindBy(id="tabMyOrders")
    WebElement orderslist;

    @FindBy(id="showMoreOrders")
    WebElement show;

    List<WebElement> list;

    public AccountOrdersPage(WebDriver driver) {
    	super(driver);
    	try {
    		//show.click();
    	}catch(NoSuchElementException ex){
    	}

    	try {
    		list = orderslist.findElements(By.xpath("div"));
    	}catch(NoSuchElementException ex) {
    	}

    }
    
    public int countOrders() {
    	try {
    		list.get(0).findElement(By.className("fa-plus")).click();
    		return list.size();
    	}catch(NullPointerException ex) {
    		return 0;
    	}
    }
    
    public Order getOrder(int index) {
    	return new Order(list.get(index).findElement(By.xpath("div[2]")));
    }
    
    public class Order{
    	WebElement span;

    	public Order(WebElement span) {
    		this.span = span;
    	}

    	public String getBrand() {
			return span.findElement(By.xpath("div[2]/div[3]/span[2]")).getText();
		}

		public String getRef() {
			return span.findElement(By.xpath("div[2]/div[3]/span[3]")).getText();
		}

		public String getSize() {
			return span.findElement(By.xpath("div[2]/div[3]/span[4]")).getText();
		}

		public String getQt() {
			return span.findElement(By.xpath("div[2]/div[3]/span[5]")).getText();
		}

		public String getPrice() {
			return span.findElement(By.xpath("div[2]/div[3]/span[6]")).getText();
		}

		public String getDisc() {
			return span.findElement(By.xpath("div[2]/div[3]/span[7]")).getText();
		}

		public String getImp() {
			return span.findElement(By.xpath("div[2]/div[3]/span[8]")).getText();
		}

		public String getState() {
			return span.findElement(By.xpath("div[2]/div[3]/span[9]")).getText();
		}
    }
}
