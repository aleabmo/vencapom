package main.java.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class ThanksPage extends NoMenuPageObject {
	WebElement info;

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(xpath="//*[@id=\"orderData\"]/span[1]/strong")
    WebElement orderId;

    @FindBy(xpath="//*[@id=\"orderData\"]/span[2]/strong")
    WebElement email;

    @FindBy(xpath="/html/body/main/div[2]/div/section[1]/div[2]/div/div/span/strong")
    WebElement numPack;    

    @FindBys(@FindBy(xpath="/html/body/main/div[2]/div/section[1]/div[2]/div/div/table"))
    List<WebElement> list_packets;

    @FindBy(id="generalSummary")
    WebElement total;

    @FindBy(id="VCES_myOrders")
    WebElement orders;

    /**
     * Constructor
     * @param driver Current driver
     */
	public ThanksPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Function which returns the order id 
	 * @return String with order id
	 */
	public String getOrderId() {
		return orderId.getText();
	}

	/**
	 * Function which returns the email from the customer
	 * @return String with customer's email
	 */
	public String getEmail() {
		return email.getText();
	}

	/** 
	 * Return de number of diferent packets
	 * @return String with the number of items
	 */
	public String getNumPack() {
		return numPack.getText();
	}
	
	/**
	 * Function which return number of packets on packets list
	 * @return int with the number of counted packets
	 */
	public int getPacketCount() {
		return list_packets.size() - 2;
	}

	/**
	 * Function which return a Packet from the Packets List
	 * @param select Integer which identifies the packet on list
	 * @return A class Packet
	 */
	public Packet getPacket(int select) {
		return new Packet(list_packets.get(select));
	}
	
	/**
	 * Function which return the payment type
	 * @return String with payment type
	 */
	public String getPaymentType() {
		return total.findElement(By.xpath("div/div[1]/div/div/span")).getText();
	}

	/**
	 * Function which return the payment type
	 * @return String with payment type
	 */
	public String getCardNumber() {
		try {
			return total.findElement(By.xpath("div/div[1]/div/div/span[2]")).getText();
		} catch(NoSuchElementException e){
			return "No Card info";
		}

	}
	/**
	 * Function which return price paid on type of payment
	 * @return String with payment expenses
	 */
	public String getTotalPaquetsPrice() {
		return total.findElement(By.xpath("div/div[2]/div/div[2]/span[1]")).getText();
	}

	/**
	 * Function which return price paid on type of payment
	 * @return String with payment expenses
	 */
	public String getPaymentPrice() {
		return total.findElement(By.xpath("div/div[2]/div/div[2]/span[2]")).getText();
	}
	
	/**
	 * Function which return the total price
	 * @return String with total order price
	 */
	public String getTotalPrice() {
		return total.findElement(By.xpath("div/div[2]/div/div[2]/span[3]")).getText();
	}
	
	/**
	 * Goes to orders page creates items according to it.
	 * @return AccountOrdersPage in order to acces its methods.
	 * @throws NoMoreOrdersException 
	 */
	public AccountOrdersPage goToFollowOrder() {
		orders.click();
		return new AccountOrdersPage(driver);
	}
	
	public class Packet {
		private WebElement table;
		List<WebElement> list_items;

		public Packet(WebElement table) {
			this.table = table;
			list_items = table.findElements(By.xpath("//tbody/tr"));
		}
		
		public String getNumPacket() {
			return table.findElement(By.xpath("//tbody/tr[1]/td/span[1]/strong")).getText();
		}
		
		public String getOrderPacket() {
			return table.findElement(By.xpath("//tbody/tr[1]/td/span[1]")).getText();
			
		}
		
		public String getSellerPacket() {
			return table.findElement(By.xpath("//tbody/tr[1]/td/span[2]")).getText();
			
		}

		/**
		 * Function which return number of items on product list
		 * @return int with the number of counted items
		 */
		public int getItemCount() {
			return list_items.size() - 2;
		}
		
		/**
		 * Function which return an Item from the Product List
		 * @param select Integer which identifies the product on list
		 * @return A class Item
		 */
		public Item getItem(int select) {
			//+1 es perque la primera row de la taula es on va les ids i demas
			return new Item(list_items.get(select + 1));
			
		}
		
		/**
		 * Function which returns the delivery address on a single string
		 * @return String containing the delivery address
		 */
		public String getAddress() {
			String result = "";
			for (WebElement aux : table.findElement(By.id("VCES_deliveryAddress")).findElements(By.xpath("span"))) {
				result = result + " " + aux.getText();
			}
			return result;
		}
		
		public String getTotalItems() {
			return table.findElement(By.id("VCES_priceTotal")).getText();
		}
		
		//list_items.size() es per tal de poder agafar lultim element de la llista no cal resta 1 ja que els xpath comencen a 1
		public String getDeliveryCost() {
			return table.findElement(By.xpath("//tbody/tr["+list_items.size()+"]/td[2]/ul/li[4]")).getText();
		}
		
		public String getTotalPacket() {
			return table.findElement(By.xpath("//tbody/tr["+list_items.size()+"]/td[2]/ul/li[6]")).getText();
		}
		
		public String getDeliveryTime() {
			return table.findElement(By.xpath("//tbody/tr["+list_items.size()+"]/td[2]/ul/li[8]")).getText();
		}
		
		public String toString() {
			return "N� Paquets: " + getNumPacket()+"\nOrderId: "+getOrderPacket()+"\nSeller: "+getSellerPacket()+"\nAddress: "+getAddress() +"\nTotal Productos: "+ getTotalItems() +"\nTotal envio: "+ getDeliveryCost()+"\nTotal paquet: "+getTotalPacket()+"\nFecha Estimada: "+getDeliveryTime();
		}
	}
	
	/***
	 * New class in order to differenciate Products
	 * @author aleixabengochea
	 *
	 */
	public class Item {
		private WebElement tr;

		/**
		 * Constructor of Item class
		 * @param tr Table row where item is located
		 */
		public Item(WebElement tr){
			this.tr = tr;
		}
		
		/**
		 * Function which return url from item image
		 * @return String with item's url
		 */
		public String getImg() {
			return tr.findElement(By.xpath("td[1]/img")).getAttribute("src");
		}

		/**
		 * Function which return brand from item
		 * @return String with item's brand
		 */
		public String getBrand() {
			return tr.findElement(By.xpath("td[2]")).getText();
		}

		/**
		 * Function which return Description from item
		 * @return String with item's description
		 */
		public String getDescription() {
			return tr.findElement(By.xpath("td[3]")).getText();
		}

		/**
		 * Function which returns size from item
		 * @return String with item's size
		 */
		public String getSize() {
			return tr.findElement(By.xpath("td[4]")).getText();
		}

		/**
		 * Function which returns quantity of current item
		 * @return String with number of same item.
		 */
		public String getQt() {
			return tr.findElement(By.xpath("td[5]")).getText();
		}

		/**
		 * Function which returns price of current item
		 * @return String with item's price
		 */
		public String getPrice() {
			return tr.findElement(By.xpath("td[6]")).getText();
		}

		/**
		 * Function which returns discount applied to item
		 * @return String with item's discount
		 */
		public String getDisc() {
			return tr.findElement(By.xpath("td[7]")).getText();
		}

		/**
		 * Function which return final price of item
		 * @return String with item's final price
		 */
		public String getImp() {
			return tr.findElement(By.xpath("td[8]")).getText();
		}
		
		@Override
		public String toString() {
			return "== Item == \nBrand: " + getBrand() + "\nDescription: " + getDescription() + "\nSize: " + getSize() + "\nQuantity: " + getQt() + "\nPrice: " + getPrice() + "\nDiscount: " + getDisc() + "\nFinalPrice: " + getImp() ;
		}
		
	}
}
