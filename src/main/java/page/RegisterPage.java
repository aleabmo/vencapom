package main.java.page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="Email")
    WebElement insertEmail;

    @FindBy(id="Password")
    WebElement insertPass;

    @FindBy(id="Name")
    WebElement insertName;

    @FindBy(id="CompleteSurname")
    WebElement insertSurname;

    @FindBy(id="BirthDate_DayOfBirth")
    WebElement dateDay;

    @FindBy(id="dropdownMenu1")
    WebElement dateMonth;

    @FindBy(id="BirthDate_YearOfBirth")
    WebElement dateYear;

    @FindBy(id="female")
    WebElement female;

    @FindBy(id="male")
    WebElement male;

    @FindBy(xpath="//label[contains(@for,'acceptConditions-visualcheck')]")
    WebElement acceptConditions;

    @FindBy(xpath="//label[contains(@for,'acceptConditionsLOPD-visualcheck')]")
    WebElement acceptConditionsLOPD;

    @FindBy(xpath="//*[@id=\"register\"]/div/form/div[4]/div/button")
    WebElement btnRegister;

    @FindBy(xpath="//span[contains(@class, 'field-validation-valid')]")
    List<WebElement> errMsg;

    private String url = "/register";

    public RegisterPage(WebDriver driver){
    	super(driver);
    }

    public void setEmail(String strEmail){
        insertEmail.sendKeys(strEmail);     
    }
    
    /**
     * Method that returns all the errors from input camps
     * @return A hasmap<String,String> containing Error Camp, Error Msg
     */
    public Map<String,String> getErrorMsg() {
    	Map<String, String> result = new HashMap<>();
    	for(WebElement aux: errMsg) {
    		if(!aux.getText().equalsIgnoreCase("")) {
    			result.put(aux.getAttribute("data-valmsg-for"), aux.getText());
    		}
    	}
    	return result;
    }
    
    /**
     * Method which sets password
     * @param strPassword 
     */
    public void setPassword(String strPassword){
    	insertPass.sendKeys(strPassword);
    }

    public void setName(String strName){
        insertName.sendKeys(strName);     
    }

    public void setSurname(String strSurname){
        insertSurname.sendKeys(strSurname);     
    }

    public void setDay(String strDay){
        dateDay.sendKeys(strDay);     
    }

    public void setMonth(String strMonth){
    	dateMonth.click();
    	List<WebElement> temp = driver.findElements(By.xpath("//*[@id=\"register\"]/div/form/div[2]/div[3]/div/div[2]/ul/li/a"));
    	for(WebElement aux : temp) {
    		if(aux.getText().equalsIgnoreCase(strMonth)) {
    			aux.click();
    			break;
    		}
    	}
    }

    public void setYear(String strYear){
        dateYear.sendKeys(strYear);     
    }
    
    public void setGender(char gender) {
    	switch(gender) {
    	case 'M':
    		male.click();
    		break;
    	case 'F':
    		female.click();
    		break;
    	default:
    		//Llen�ar error
    	}
    		
    }
    
    public void setConditions(){
    	acceptConditions.click();
    }  

    public void setLOPD(){
    	acceptConditionsLOPD.click();
    }  
    
    public HomePage clickRegister(){
    	btnRegister.click();
    	return new HomePage(driver);
    }  

    /**
     * This POM method will be exposed in test case to login in the application
     * @param strEmail
     * @param strPasword
     * @return
     */
    public HomePage registerToVenca(String strEmail,String strPasword, String strName, String strSurname, String strDay, String strMonth, String strYear, char gender){
        this.setEmail(strEmail);
        this.setPassword(strPasword);
        this.setName(strName);
        this.setSurname(strSurname);
        this.setDay(strDay);
        this.setMonth(strMonth);
        this.setYear(strYear);
        this.setGender(gender);
        this.setConditions();
        //this.setLOPD();
        return clickRegister();           

    }

}