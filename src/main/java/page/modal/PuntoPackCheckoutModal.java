package main.java.page.modal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import main.java.page.CheckoutDeliveryPage;
import main.java.page.PageObject;
import utils.Constants;

import java.util.List;


public class PuntoPackCheckoutModal extends PageObject {

    @FindBy(xpath="//*[@id=\"mapmodals\"]/div/div/div[1]/button")
    private WebElement btnClose;

    @FindBy(id="bigPostalCode")
    private WebElement inputCp;

    @FindBy(id="bigCity")
    private WebElement inputCity;

    @FindBy(id="findPackPoint")
    private WebElement btnSearch;

    @FindBy(xpath="//*[@id=\"packPointResults\"]/div/div/div")
    private List<WebElement> listPack;

    private CheckoutDeliveryPage page;

    public  PuntoPackCheckoutModal(CheckoutDeliveryPage page){
        super(page.getDriver());
        this.page = page;

    }

    public CheckoutDeliveryPage clickClose(){
        btnClose.click();
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(
                ExpectedConditions.invisibilityOfElementLocated(By.id("mapmodals"))
        );
        return this.page;
    }

    public void setCp(String cp){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(
                ExpectedConditions.visibilityOf(inputCp)
        );
        inputCp.clear();
        inputCp.sendKeys(cp);
    }

    public void setCity(String city){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(
                ExpectedConditions.visibilityOf(inputCity)
        );
        inputCity.clear();
        inputCity.sendKeys(city);
    }

    public void clickSearch(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(
                ExpectedConditions.visibilityOf(btnSearch)
        );
        btnSearch.click();
        wait.until(ExpectedConditions.or(
                ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id=\"packPointResults\"]/div[@class='text-center']"))
        ));
        wait.until(ExpectedConditions.or(
//                ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"packPointResults\"]/div/div/div[1]/div/a")),
                ExpectedConditions.visibilityOfAllElements(listPack),
                ExpectedConditions.visibilityOfElementLocated(By.id("packpoint-noresults"))
        ));
    }

    public int getNumOptions(){
        return listPack.size();
    }

    public String getOption(int index){
        return listPack.get(index).getText();
    }

    public CheckoutDeliveryPage setOption(int index){
        listPack.get(index).click();
        return this.page;
    }
}
