package main.java.page.modal;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import main.java.page.CheckoutDeliveryPage;
import main.java.page.PageObject;

public class AddressCheckoutModal extends PageObject {

    @FindBy(className="close")
    private WebElement btnClose;

    @FindBy(id="Name")
    private WebElement inputTitle;

    @FindBy(id="Address_Street")
    private WebElement inputAddress;

    @FindBy(id="Address_Number")
    private WebElement inputNum;

    @FindBy(id="Address_Additional")
    private WebElement inputComplement;

    @FindBy(id="Address_City")
    private WebElement inputCity;

    @FindBy(id="Address_PostalCode")
    private WebElement inputCP;

    @FindBy(id="Address_Province")
    private WebElement inputProv;

    @FindBy(xpath="//*[@id=\"changeDeliveryAddress\"]/div/div/div[2]/div[3]/button[1]")
    private WebElement btnCancel;

    @FindBy(xpath="//*[@id=\"changeDeliveryAddress\"]/div/div/div[2]/div[3]/button[2]")
    private WebElement btnAdd;

    private CheckoutDeliveryPage page;

    public AddressCheckoutModal(CheckoutDeliveryPage page) {
        super(page.getDriver());
        this.page = page;
    }

    /**
     * Method for closing modal
     */
    public CheckoutDeliveryPage closeModal() {
        btnClose.click();
        return this.page;
    }

    /**
     * Method for setting title
     * @param title String with title
     */
    public void setTitle(String title){
        inputTitle.sendKeys(title);
    }

    /**
     * Method for setting address
     * @param address String with address
     */
    public void setAddress(String address){
        inputAddress.sendKeys(address);
    }

    /**
     * Method for setting number
     * @param num String with number
     */
    public void setNum(String num){
        inputNum.sendKeys(num);
    }

    /**
     * Method for setting optional
     * @param com String with optional
     */
    public void setComplemen(String com){
        inputComplement.sendKeys(com);
    }

    /**
     * Method for setting city
     * @param city String with city
     */
    public void setCity(String city){
        inputCity.sendKeys(city);
    }

    /**
     * Method for setting postal code
     * @param cp String with postal code
     */
    public void setCP(String cp){
        inputCP.sendKeys(cp);
    }

    /**
     * Method for setting province
     * @param prov String with province
     */
    public void setProv(String prov){
        inputProv.sendKeys(prov);
    }

    /**
     * Method for cancel input of address
     */
    public CheckoutDeliveryPage clickCancel(){
        btnCancel.click();
        return this.page;
    }

    public void clearAll() {
        if(inputTitle.getText()!="")
            inputTitle.clear();
        if(inputAddress.getText()!="")
            inputAddress.clear();
        if(inputNum.getText()!="")
            inputNum.clear();
        if(inputComplement.getText()!="")
            inputComplement.clear();
        if(inputCity.getText()!="")
            inputCity.clear();
        if(inputCP.getText()!="")
             inputCP.clear();
        if(inputProv.getText()!="")
            inputProv.clear();
    }

    /**
     * Method for adding address
     */
    public CheckoutDeliveryPage clickAddAddress(){
        btnAdd.click();
        return this.page;
    }
}
