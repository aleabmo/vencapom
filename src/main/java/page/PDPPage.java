package main.java.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.java.exception.NoProductException;
import utils.DropDown;

/**
 * Class que modeliza el escaparate
 * 
 * @author aleixabengochea
 *
 */
public class PDPPage extends MenuPageObject{

    /**
     * All WebElements are identified by @FindBy annotation
     */

    //Numero total de productos en el escaparate
    @FindBy(id="totalProducts")
    WebElement nProducts;
    
    //Identifica el titulo del escaparate
    @FindBy(xpath="//h1")
    WebElement title;

    //Identifica la descripcion del escaparate
    @FindBy(xpath="//h2")
    WebElement description;

    //Identifica la descripcion del escaparate
    String btnDropdown = "//*[@id=\"summaryResults\"]/div[2]/div";

    //Seccion que contiene todos lod productos. Esta separado para que sea mas modular.
    Section prodSection;
    
    //Seccion que contiene el dropdown de orden.
    DropDown dropdown;
    
    Product product;

    /**
     * Constructor 
     * @param driver
     */
    public PDPPage(WebDriver driver){
        super(driver);
        prodSection = new Section(driver);
        dropdown = new DropDown(driver, btnDropdown);
    }

    /**
     * Funcion que devuelve el n�mero de productos en el escaparate.
     * @return number of products
     */
    public int getNumProd() {
    	return Integer.parseInt(nProducts.getText());
    }
    
    /**
     * Funcion que devuelve el Titulo del escaparate.
     * @return String containing title
     */
    public String getHeader() {
    	return title.getText();
    }
    
    /**
     * Funcion que devuelve la Descripcion del escaparate.
     * @return String containing description.
     */
    public String getDescription() {

    	return description.getText();
    }

    /**
     * Fucntion for setting a product and not getting it every time.
     * Just implemented for time consuming
     * @param index Index of product you wanna select.
     * @throws NoProductException Exception if no product is fount with this index
     */
    public void setProduct(int index) throws NoProductException {
    	product = this.getProduct(index);
    }

    /**
     * Function that return a product from escaparate on the given index
     * @param index Position of item 
     * @return The product if found
     * @throws NoProductException Exception if no product is found with this index
     */
    public Product getProduct(int index) throws NoProductException {
    	if(index < getNumProd()) {
    		//Avancem tantes pagines, sabent que hi ha 84 items per pagina.
    		int pages = index / 84;
    		int offset = index % 84;
    		for(int i=0; i<pages; i++) {
    			prodSection = prodSection.getMoreProd();
    		}
    		return prodSection.getProduct(offset);
    	}
    	else throw new NoProductException("Index out of number of products");
    }  
    
    //Funcions del dropdown
    /**
     * Funcion que devuelve el listado de opciones del dropdown
     * @return List of options as String
     */
    public List<String> getDropdown(){
    	return dropdown.listElem();
    }
    
    /**
     * Funcion que permite ordenar el escaparate
     * @param optn String cotaining optin
     */
    public void orderBy(String optn) {
    	dropdown.getOption(optn);;
    }
    
    
    /* ===============================
     * Metodos de producto
     * ===============================
     * Al intentar hacer esto un poco blackbox,
     * nos vemos obligados a implementar metodos
     * para que el objecto product sea totalmente
     * opaco
     * Los metodos estan duplicados haciendo un overraiding
     * para que se pueda presetear un producto i no tener que llamar
     * al getproduct todo el tiempo.
    */
    
    public String getImgProd(int index) throws NoProductException {
    	return getProduct(index).getImg();
    }
    public List<String> getTagsProd(int index) throws NoProductException {
    	return getProduct(index).getTags();
    }
    public String getBrandProd(int index) throws NoProductException {
    	return getProduct(index).getBrand();
    }
    public String getDescProd(int index) throws NoProductException {
    	return getProduct(index).getDesc();
    }
    public String getPriceProd(int index) throws NoProductException {
    	return getProduct(index).getPrice();
    }
    public void addProdToWish(int index) throws NoProductException {
    	getProduct(index).addToWish();
    }
    public PLPPage clickProd(int index) throws NoProductException {
    	return getProduct(index).click();
    }
    
    public String getImgProd() {
    	return product.getImg();
    }
    
    public List<String> getTagsProd(){
    	return product.getTags();
    }
    
    public String getBrandProd() {
    	return product.getBrand();
    }
    
    public String getDescProd() {
    	return product.getDesc();
    }

    public String getPriceProd() {
    	return product.getPrice();
    }
    
    public void addProdToWish() {
    	product.addToWish();
    }
    
    public PLPPage clickProd() {
    	return product.click();
    }
    
    private class Section {
    	private WebDriver driver;
    	
    	// Lista de todos los productos en el escaparate
    	@FindBy(xpath="//div[contains(@class, 'productCounter')]")
    	List<WebElement> product;

    	// Identifica el boton de pagina siguiente en el listado de productos.
    	@FindBy(xpath="//*[@id=\"results\"]/section[2]/div[3]/a")
    	WebElement btnNext;

    	private Section(WebDriver driver){
    		this.driver = driver;
    		PageFactory.initElements(driver, this);
    	}

    	private Section getMoreProd() {
    		//Hay que poner un wait ya que al ser llamadas ajax puede ser que los elementos aun no esten cargados
    		WebDriverWait wait = new WebDriverWait(driver, 60); 
    		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(btnNext));
    		element.click();
    		return new Section(driver);
    	}
    	
    	
    /*	
     //Te pinta de ser un metode que no es necessita. Ademes dona problemes el wait.
    	private List<WebElement> getListProd(){
    		//Hay que poner un wait ya que al ser llamadas ajax puede ser que los elementos aun no esten cargados
    		WebDriverWait wait = new WebDriverWait(driver, 60); 
    		List<WebElement> element = wait.until(ExpectedConditions.visibilityOfAllElements(product));
    		return element;
    	}
    */
    	
    	private Product getProduct(int offset) {
    		WebDriverWait wait = new WebDriverWait(driver, 50); 
    		//WebElement element = wait.until(ExpectedConditions.stalenessOf(product.get(offset)));
    		if(wait.until(ExpectedConditions.elementSelectionStateToBe(product.get(offset), false))){
    			return new Product(product.get(offset));
    		}else
    			return new Product(product.get(offset));
    	}

    }
    
    /**
     * Class used to model and add functionality to a product view from pdp.
     * @author aleixabengochea
     *
     */
    private class Product {
    	//Product itself
    	private WebElement product;

    	private Product(WebElement product) {
    		this.product = product;
    	}
    	
    	/**
    	 * Retrieve all the tags of this product.
    	 * @return List of strings with all the tags name.
    	 */
    	private List<String> getTags(){
    		List<String> result = new ArrayList<>();
    		List<WebElement> tags = product.findElements(By.xpath(".//div[contains(@class, 'tagsBox')]/div"));
    		for(WebElement aux : tags) {
    			result.add(aux.getAttribute("class"));
    		}
    		return result;
    	}
    	
    	/**
    	 * Retrieve the name of the product's brand.
    	 * @return String of the brand's name.
    	 */
    	private String getBrand() {
    		return product.findElement(By.xpath("//strong/span[contains(@class, 'bold storeDescProd thin')]")).getText();
    	}
    	
    	/**
    	 * Retrieve the description of the product.
    	 * @return String containing description
    	 */
    	private String getDesc() {
    		return product.findElement(By.xpath("//div/span[contains(@class, 'storeDescProd thin')]")).getText();
    	}
    	
    	/**
    	 * Returns the price of the product.
    	 * @return String showing product price.
    	 */
    	private String getPrice() {
    		String result = "";
    		List<WebElement> price = product.findElements(By.xpath(".//span[contains(@class, 'storeFrontsFinalPrice')]/span"));
    		for(WebElement aux : price) {
    			result = result+','+aux.getText();
    		}
    		return result;
    	}
    	
    	/**
    	 * Get href of the product's img.
    	 * @return Href of img
    	 */
    	private String getImg() {
    		return product.findElement(By.xpath(".//img")).getAttribute("src");
    	}
    	
    	//Pendents de implementar
    	/*
    	private List<String> getColors(){
    		
    	}
    	
    	private void changeColor() {
    		
    	}
    	*/
    	
    	/**
    	 * Method to add item to wishlist.
    	 */
    	private void addToWish() {
    		WebDriverWait wait = new WebDriverWait(driver, 50); 
    		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(product.findElement(By.xpath(".//button"))));
    		element.click();
    	}
    	
    	private PLPPage click() {
    		product.click();
    		return new PLPPage(driver);
    	}
    }
    
  /*  private class SidePanel{
    	private WebDriver driver;

    	@FindBy(id="accordionFilters")
    	WebElement filter;

    	@FindBy(id="accordionCategories")
    	WebElement categories;

    	@FindBy(xpath="//*[@id=\"accordionFilters\"]/div[1]/span[2]/a")
    	WebElement cleanAll;

    	private SidePanel(WebDriver driver) {
    		this.driver = driver;
    		PageFactory.initElements(driver, this);
    	}
    	
    	private void cleanAllFilters() {
    		cleanAll.click();
    	}
    	
    }*/
}