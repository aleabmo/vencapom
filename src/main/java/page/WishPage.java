package main.java.page;

import org.openqa.selenium.WebDriver;

public class WishPage extends MenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    private String url = "/WishList";

    public WishPage(WebDriver driver){
    	super(driver);
    }
   
}