package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;

public class LoginPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="EmailLogin")
    WebElement email;

    @FindBy(id="Password")
    WebElement pass;    

    @FindBy(xpath="/html/body/main/section/div[1]/form/div[3]/button")
    WebElement btnLogin;

    @FindBy(xpath="/html/body/main/section/div[1]/form/div[4]/div[3]/img")
    WebElement btnLoginFb;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[2]/div[1]/button")
    WebElement btnGuest;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[2]/div/button")
    WebElement btnCreate;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[3]/div[3]/img")
    WebElement btnCreateFb;

    @FindBy(xpath="/html/body/main/section/div[1]/form/div[3]/span/p")
    WebElement btnRecovery;
    
    //Object pre;
    private String url = "/login";
    
    public LoginPage(WebDriver driver){
    	super(driver);
    	//pre = new HomePage(driver);
    }
    
    /* Overcharging constructor in order to use other no homepage
    public LoginPage(WebDriver driver, Object camefrom) {
    	super(driver);
    	pre = camefrom;
    }
    */
    

    /**
     * Method for setting login email
     * @param strEmail Email you wanna use for login
     */
    public void setEmail(String strEmail){
        email.sendKeys(strEmail);     
    }

    /**
     * Method for setting login password
     * @param strPassword password you wanna use for login
     */
    public void setPassword(String strPassword){
    	pass.sendKeys(strPassword);
    }

    /**
     * Click on login button
     * You should return where you came from not only homepage. Not easy to design.
     * Partial implementation dont know if can be better.
     * @return Object where you came from there is a need to cast
     */
    public HomePage clickLogin(){
    	btnLogin.click();
    	return new HomePage(driver);
    }  

    /**
     * Method that sends driver to RegisterPage
     * @return RegisterPage instance in order to see new methods
     */
    public RegisterPage goToRegister(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(btnCreate));
    	btnCreate.click();
        return new RegisterPage(driver);
    }  
    
    /**
     * Method that sends driver to RecoveryPage
     * @return RecoveryPage instance in order to see new methods.
     */
    public RecoveryPage gotToRecovery() {
    	btnRecovery.click();
        return new RecoveryPage(driver);
    	
    }

    /**
     * This POM method will be exposed in test case to login in the application
     * Probably this method is gonna be in another abstraction layer.
     * @param strEmail
     * @param strPasword
     * @return
     */
    public HomePage loginToVenca(String strUserName,String strPasword){
        this.setEmail(strUserName);
        this.setPassword(strPasword);
        return clickLogin();           
    }

}