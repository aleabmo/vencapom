package main.java.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewsletterPage extends NoMenuPageObject {

    /**
     * All WebElements are identified by @FindBy annotation
     */

    @FindBy(id="EmailLogin")
    WebElement email;

    @FindBy(id="Password")
    WebElement pass;    

    @FindBy(xpath="/html/body/main/section/div[1]/form/div[3]/button")
    WebElement btnLogin;

    @FindBy(xpath="/html/body/main/section/div[1]/form/div[4]/div[3]/img")
    WebElement btnLoginFb;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[2]/div[1]/button")
    WebElement btnGuest;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[2]/div/button")
    WebElement btnCreate;

    @FindBy(xpath="/html/body/main/section/div[2]/form/div[3]/div[3]/img")
    WebElement btnCreateFb;

    public NewsletterPage(WebDriver driver){
    	super(driver);
    }

    //Set user name in textbox
    public void setEmail(String strEmail){
        email.sendKeys(strEmail);     
    }

    //Set password in password textbox
    public void setPassword(String strPassword){
    	pass.sendKeys(strPassword);
    }

    //Click on login button
    public void clickLogin(){
            btnLogin.click();
    }  

    public RegisterPage goToRegister(){
            btnCreate.click();
            return new RegisterPage(driver);
    }  

    /**
     * This POM method will be exposed in test case to login in the application
     * @param strEmail
     * @param strPasword
     * @return
     */
    public HomePage loginToVenca(String strUserName,String strPasword){
        this.setEmail(strUserName);
        this.setPassword(strPasword);
        this.clickLogin();           
        return new HomePage(driver);

    }

}